#!/bin/bash
# =========================================================================================
# definitions of variables, and functions
VERSION=0.2
# default values
POSITIONAL_ARGS=()
PROGRAM_NAME=$0

print_help()
{
    echo "Program description: "
    echo Usage:
    echo "$PROGRAM_NAME "
    echo -e "\t-h, --help            : this help"
    echo -e "\t-v, --version         : version"
}

# =========================================================================================
# parse input arguments

while [[ $# -gt 0 ]]; do
    case $1 in
    -h|--help)
    print_help
    exit 0
    ;;
    -v|--version)
    echo "Version $VERSION"
    exit 0
    ;;
    #------------------------------------------------------------------
    # Put your arguments here

    #------------------------------------------------------------------
    -*|--*)
    echo "ERROR: Unknown option $1"
    echo "INFO: see --help or -h for info how to use it"
    exit 1
    ;;
    #------------------------------------------------------------------ *)
    *)
    POSITIONAL_ARGS+=("$1") # save positional args
    shift # past argument
    ;;
    esac
done

# Change according to your needs
if [ "${#POSITIONAL_ARGS[@]}" -ge 1 ]; then
  echo "ERROR: Positional argument not expected"
fi
