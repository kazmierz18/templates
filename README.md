# Templates
Collection of templates that can be used to bootstrap some projects

In vim create new file lets say: main.c, then you can can in normal mode use:
```
:read path/to/templates/c/main.c
```
This will insert content of template file inside of yours file

# Current templates:
1. [Bash](./bash/README.md)
1. [C](./c/README.md)
