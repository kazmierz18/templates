#pragma once
/*
Copyright 2022 kazmierz18

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/


/**
 * Collection of logging macros for C simple projects
 *
 * These macros are veru useful when using vim make flow.
 * For each ERROR and TODO log, line and file is printed,
 * and it can be parsed by vim (check vim errorformat)
 * This allows to quickly jump to ERROR, or not implemented code TODO
 * See each macro doc for more information
 */

#include <stdio.h>
#include <stdlib.h>

/**
 * helper functions for int to string conversion
 */
#define STR_HELPER(x) #x
#define STR(x) STR_HELPER(x)

/**
 * Generate string with location that can be parsed by vim
 */
#define LOCATION __FILE__ ":" STR(__LINE__) ": "

/**
 * Prints error message to stderr wit LOCATION used in vim build flow
 */
#define ERROR(msg, ...)                                                        \
  (void)fprintf(stderr, LOCATION "ERROR: " msg __VA_OPT__(, ) __VA_ARGS__)
/**
 * Prints info message to stderr wit LOCATION used in vim build flow
 */
#define INFO(msg, ...)                                                         \
  (void)fprintf(stderr, "INFO: " msg __VA_OPT__(, ) __VA_ARGS__)

/**
 * Prints info message to stderr that program flow is not implemented
 * and stop execution of program
 */
#define TODO()                                                                 \
  do {                                                                         \
    (void)fprintf(stderr, LOCATION "NOT IMPLEMENTED!!\n");                     \
    exit(-1);                                                                  \
  } while (0)

/**
 * Prints error message to stderr that program flow cant handle situation
 * and stop execution of program
 */
#define PANIC()                                                                \
  do {                                                                         \
    (void)fprintf(stderr, LOCATION "at function: %s PANICKED!!\n", __func__);  \
    exit(-1);                                                                  \
  } while (0)
